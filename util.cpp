#include "util.hpp"

namespace util {
    std::vector<std::string> parse_args(std::string arg) {
        // This function should: parse a string and split it, returning a vector
        
        // Turns arg into a stringvec of words - args
        std::string token;
        std::vector<std::string> args;
        std::stringstream istream(arg);
        while (istream) //for icecream
        {
            std::getline(istream, token, ' '); //whoops
            args.push_back(token);
        }
        
        int idx = 0;
        std::vector<std::string> real_args;
        bool tail = false;
        for (std::string arg : args) { // this line
            if (!tail) {
                if (arg[0] == ':') {
                    if (idx == 0) {
                        real_args.push_back(arg.substr(1));
                        idx++;
                    } else {
                        real_args.push_back(arg);
                        tail = true;
                    }
                } else {
                    real_args.push_back(arg);
                    idx++;
                }
            } else {
                real_args[idx] += " "+arg;
            }   
        }
        return real_args;
    }
}

//int main(int argc, char *argv[]) {
//    std::vector<std::string> args(argv + 1, argv + argc);
//    for (std::string rarg : util::parse_args(args)) {
//        std::cout << rarg << " " << std::endl;
//    }
//    return 0;
//}