CXX=clang++
CXXFLAGS=-std=c++11 -O2 -ggdb -Wall -Wextra
LDFLAGS=
LIBRARIES=
SOURCES= \
 main.cpp \
 util.cpp \
 ircsocket.cpp
OBJECTS=$(SOURCES: .cpp=.o)
EXECUTABLE=cppbot

all: $(EXECUTABLE)

$(EXECUTABLE): $(OBJECTS)
	$(CXX) $(CXXFLAGS) $(INCLUDES) -o $@ $(OBJECTS) $(LDFLAGS) $(LIBRARIES)

.cpp.o:
	$(CXX) $(CXXFLAGS) $(INCLUDES) -c $< -o $@

clean:
	$(RM) $(EXECUTABLE)
