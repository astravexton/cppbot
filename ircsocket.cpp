#include "ircsocket.hpp"

IRCSocket::IRCSocket()
{
    sock = -1;
    port = 0;
    address = "";
}

bool IRCSocket::conn(std::string address, int port)
{
    if (sock == -1)
    {
        sock = socket(AF_INET , SOCK_STREAM , 0);
        if (sock == -1)
        {
            perror("Could not create socket");
        }
        std::cout << "Socket created" << std::endl;
    }

    struct addrinfo hints, *ai;
    memset(&hints, 0, sizeof hints);
    hints.ai_family = AF_INET;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_flags = AI_PASSIVE;

    if (getaddrinfo(address.c_str(), std::to_string(port).c_str(), &hints, &addrinfopointer))
    {
        std::cout << "Failed to lookup host" << std::endl;
        return false;
    }
    for ( ai = addrinfopointer; ai != NULL; ai = ai->ai_next )
    {
        struct sockaddr_in *ipv4 = (struct sockaddr_in *)ai->ai_addr;
        if (connect(sock, (struct sockaddr*)ipv4, ai->ai_addrlen) == 0)
        {
            break;
        }
        perror("connect failed. trying next ip");
        continue;
    }
    if (ai == NULL)
    {
        // connection failed entirely
        return false;
    }
    std::cout << "Connected" << std::endl;
    return true;
}

bool IRCSocket::send_data(std::string data)
{
    //Send some data
    data += "\r\n";
    if (send(sock, data.c_str(), strlen(data.c_str()), 0) < 0)
    {
        perror("Send failed : ");
        return false;
    }
    std::cout << "Data send\n";
    return true;
}

std::string getline(std::istream& is) // yeah.
{
    std::string line;
    char c;
    while (is >> c)
    {
        if (c != '\n')
        {
            line += c;
        }
        else
        {
            return line;
        }
    }
    return "";
}

std::vector<std::string> IRCSocket::receive()
{
    std::vector<std::string> lines;
    char buffer[4096] = {0};
    int len = recv(sock, buffer, sizeof(buffer), 0);
    if (len <= 0) return {};
    std::string data = overflow + std::string(buffer, len);
    overflow = std::string();
    while(true)
    {
        auto pos = data.find('\n');
        if (pos != std::string::npos)
        {
            std::string line = std::string(data, 0, pos-1);
            lines.push_back(line);
            data = data.substr(pos+1);
        }
        else
        {
            overflow = data;
            return lines;
        }
    }
}
