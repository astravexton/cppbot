#include <iostream>
#include <string>
#include <vector>
#include <cstring>
#include <netdb.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sstream>

#include <istream>


class IRCSocket {
private:
    int sock;
    std::string address;
    int port;
    std::string overflow;
    struct addrinfo *addrinfopointer;
     
public:
    IRCSocket();
    bool conn(std::string, int);
    bool send_data(std::string data);
    std::vector<std::string> receive();
};
