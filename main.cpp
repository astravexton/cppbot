#include "ircsocket.hpp"
#include "util.hpp"

int main(int argc, char *argv[]) {
    if (argc == 4) {
        std::vector<std::string> args(argv + 1, argv + argc);
        std::string host = args[0];
        int port = stoi(args[1]);
        std::string chan = args[2];
        IRCSocket c;
        c.conn(host, port);
        c.send_data("USER cppbot * * :cppbot");
        c.send_data("NICK cppbot");
        while (true) {
            std::vector<std::string> lines = c.receive();
            for (std::string line : lines) {
                std::cout << "> " << line << std::endl;
                std::vector<std::string> args = util::parse_args(line);
                if (args[0] == "PING")
                {
                    c.send_data("PONG "+args[1]);
                }
                if (args[1] == "MODE")
                {
                    c.send_data("JOIN "+chan);
                }
            }
        }
    } else {
        std::cout << "Error: not enough arguments" << std::endl;
    }
    return 0;
}
